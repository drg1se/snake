## Basic introduction
Just another snake from just another newbie\
Required interpreter : Python 3.5+\
Required libraries: tkinter, numpy\
To run the game having all required libraries installed, run the following from 
your command shell being in the project directory:
```
python game.py 
```
To specify the parameters of the field(window) 
use optional parameters `--width` and `--height`:
```
python game.py --width <width> --height <height>
```
To install the required libraries:
```
pip install numpy
pip install tkinter 
```
---
## Structure
`snake.py` -- file containing the `Snake` class, can be used without tkinter library\
`game.py` -- file with the game itself. It contains `Game` class.\
## Some explanations
Class `Game` inherits from `tkinter.Frame` and therefore can be placed at any other Frame
and actually works just as any other tkinter object

As all graphics are built using labels and grid, setting window size not more than 2100 
points in total is recommended.
## Further versions
To extend the maximum size of the window at which performance remains solid rewriting all
graphics from labels and grid to canvas is scheduled
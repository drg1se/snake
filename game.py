import tkinter as tk
from numpy.random import rand
from snake import Snake
from argparse import ArgumentParser


class Game(tk.Frame):
    def __init__(self, master, x=50, y=50, **kwargs):
        tk.Frame.__init__(self, master)
        self.__master = master
        self.focus_set()
        self.points = [[tk.Label(self, height=1, width=2, bg='black', fg='white') for j in range(y)] for i in range(x)]
        for i in range(x):
            for j in range(y):
                self.points[i][j].grid(row=j, column=i)
        self.__snake = Snake(x=x, y=y)
        self.__x = x
        self.__y = y
        # Doesn't work yet. Supposed to keep some keyboard sequence that in some cases will let the user do some cheating
        self.__god_sequence = kwargs.get('god_sequence', 'g-o-d')
        self.paused = False
        self.__gen_food = lambda: (int((rand()*(x - 1))), int(rand()*(y - 1)))
        self.food = self.__gen_food()
        self.points[self.food[0]][self.food[1]].configure(bg='red')
        self.bind('<Up>', lambda event: self.__snake.turn(1))
        self.bind('<Left>', lambda event: self.__snake.turn(0))
        self.bind('<Right>', lambda event: self.__snake.turn(2))
        self.bind('<Down>', lambda event: self.__snake.turn(3))
        self.bind('w', lambda event: self.__snake.turn(1))
        self.bind('a', lambda event: self.__snake.turn(0))
        self.bind('d', lambda event: self.__snake.turn(2))
        self.bind('s', lambda event: self.__snake.turn(3))
        self.bind('p', lambda event: self.__change_pause())
        self.game_loop()

    def fail(self):
        print('Score:', len(self.__snake.tail))
        fail_window = tk.Tk()
        fail_window.geometry('10x10')
        fail_window.focus_set()
        score = tk.Label(fail_window, text='You scored: '+str(len(self.__snake.tail))+'!')
        restart = tk.Button(fail_window, text='Restart')
        quit_button = tk.Button(fail_window, text='Quit')
        score.pack(side=tk.TOP)
        restart.pack(side=tk.LEFT)
        quit_button.pack(side=tk.RIGHT)

        # mentioned before case of cheating. Contains some bugs
        def god(event):
            print('god sequence entered')
            fail_window.destroy()
            self.__snake.head = self.__snake.tail.pop()
            self.__snake.reverse()
            print('new head:', self.__snake.head)
            print('new tail', *self.__snake.tail)
            self.game_loop()

        def qquit(event):
            fail_window.destroy()
            self.__master.destroy()

        def rrestart(event):
            self.focus_set()
            fail_window.destroy()
            self.restart()

        fail_window.bind(self.__god_sequence, god)
        restart.bind('<Button-1>', rrestart)
        fail_window.bind('r', rrestart)
        quit_button.bind('<Button-1>', qquit)

    def restart(self):
        print('Restarting...')
        print('Clearing the map...')
        if self.__snake.head in self.__snake.tail:
            self.points[self.__snake.head[0]][self.__snake.head[1]].configure(bg='black')
        for point in self.__snake.tail:
            self.points[point[0]][point[1]].configure(bg='black')
        print('Creating a new snake...')
        self.__snake = Snake(x=self.__x, y=self.__y)
        print('Rerendering the field...')
        self.render(food=True)
        print('Starting game loop')
        self.game_loop()
        print('New gameloop started')

    def __change_pause(self):
        self.paused = not self.paused

    def pause(self):
        if self.paused:
            self.after(500, self.pause)
        else:
            self.game_loop()

    # the main body of the game
    def game_loop(self):
        tail_left = self.__snake.tail[0]
        if self.__snake.move(self.food):
            # guarantees that the food will not spawn in snake
            while self.food in [self.__snake.head] + self.__snake.tail:
                self.food = self.__gen_food()
            self.points[self.food[0]][self.food[1]].configure(bg='red')
            print('New food spawned at ', *self.food)
        else:
            # if no food found removes the last part of the tail from the field (graphically)
            self.points[tail_left[0]][tail_left[1]].configure(bg='black')
        if self.__snake.alive:
            self.render()
            self.after(100, self.pause if self.paused else self.game_loop)
        else:
            self.fail()

    def render(self, **params):
        if params.get('food', False):
            self.points[self.food[0]][self.food[1]].configure(bg='red')
        self.points[-1][-1].configure(text=str(len(self.__snake.tail)))
        self.points[self.__snake.head[0]][self.__snake.head[1]].configure(bg='green')
        for part in self.__snake.tail:
            self.points[part[0]][part[1]].configure(bg='white')


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('--width', default=30, type=int)
    parser.add_argument('--height', default=30, type=int)
    args = parser.parse_args()
    root = tk.Tk()
    label = tk.Label(root, text='The Snake game')
    game = Game(root, args.width, args.height)
    label.pack()
    game.pack()
    root.mainloop()


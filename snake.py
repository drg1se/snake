import numpy as np


class Snake:
    def __init__(self, **mapsize):
        x = mapsize.get('x', 50)
        y = mapsize.get('y', 50)
        self.direction = int(np.random.rand()*3.9) # 0 - left, 1 -up, 2 - right, 3 - down
        self.head = (int(x/3 + np.random.rand()*x/3), int(y/3 + np.random.rand()*y/3))
        self.tail = [(
            self.head[0] - (1 if self.direction == 2 else -1 if self.direction == 0 else 0),
            self.head[1] + (1 if self.direction == 1 else -1 if self.direction == 3 else 0)
        )]
        self.__critical_x = mapsize.get('x', 50)
        self.__critical_y = mapsize.get('y', 50)
        self.alive = True

    def reverse(self):
        adjacent = self.tail[:2] if len(self.tail) > 1 else [self.tail[0], self.head]
        self.tail[0], self.head = self.head, self.tail[0]
        if adjacent[0][0] == adjacent[1][0]:
            self.direction = 2 + adjacent[0][1] - adjacent[1][1]
        else:
            self.direction = 1 + adjacent[1][0] - adjacent[1][1]
        self.tail.reverse()

    def not_in_wall(self):
        return -1 < self.head[0] < self.__critical_x and -1 < self.head[1] < self.__critical_y

    def turn(self, direction):
        dx = 0 if direction % 2 else 1 if direction == 2 else -1
        dy = 0 if dx else 1 if direction == 3 else -1
        if self.tail[-1] != (self.head[0] + dx, self.head[1] + dy):
            self.direction = direction

    def move(self, *food):
        self.tail.append(self.head)
        self.head = (
            self.head[0] + (1 if self.direction == 2 else -1 if self.direction == 0 else 0),
            self.head[1] - (1 if self.direction == 1 else -1 if self.direction == 3 else 0)
        )
        if self.head not in food:
            del self.tail[0]
        self.alive = not (self.head in self.tail) and self.not_in_wall()
        return self.head in food


